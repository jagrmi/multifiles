module gitlab.com/jagrmi/multifiles

go 1.15

require (
	github.com/micro/go-micro v1.18.0 // indirect
	gitlab.com/gitlab-org/project-templates/go-micro v0.0.0-20190225134054-1385fab987bc // indirect
)
