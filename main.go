package main

import (
	// "context"
	// "log"

	"gitlab.com/jagrmi/multifiles/foo"
)

// 	proto "gitlab.com/gitlab-org/project-templates/go-micro/proto"
// 	"github.com/micro/go-micro"
// )

// type Greeter struct{}

// func (g *Greeter) Hello(ctx context.Context, req *proto.HelloRequest, rsp *proto.HelloResponse) error {
// 	rsp.Greeting = "Hello " + req.Name
// 	return nil
// }

// func main() {
// 	service := micro.NewService(
// 		micro.Name("greeter"),
// 	)

// 	service.Init()

// 	proto.RegisterGreeterHandler(service.Server(), new(Greeter))

// 	if err := service.Run(); err != nil {
// 		log.Fatal(err)
// 	}
// }
func main() {
	foo.Bar()
}

-- go.mod --
module gitlab.com/jagrmi/multifiles

-- foo/foo.go --
package foo

import "fmt"

func Bar() {
	fmt.Println("This function lives in an another file!")
}